import sys
import pandas as pd
import numpy as np
from sqlalchemy import create_engine

def load_data(messages_filepath, categories_filepath):
    '''
    INPUT:
    messages_filepath (string) - a directory containing the messages.csv
    categories_filepath (string) -  a directory containing the categories.csv
    
    OUTPUT:
    df (pandas dataframe) - a dataframe merged from messages.csv and categories.csv
    
    '''

    # load messages dataset
    messages = pd.read_csv(messages_filepath)

    # load categories dataset
    categories = pd.read_csv(categories_filepath)

    # merge datasets
    df = categories.merge(messages, how="left", on=["id"])
    
    return df 

def clean_data(df):
    '''
    INPUT:
    df (pandas dataframe) - a dataframe merged from messages.csv and categories.csv
    
    OUTPUT:
    df (pandas dataframe) - a dataframe after data cleaning
    
    DESCRIPTION :
    Make categories into separate category columns and remove duplicates

    '''

    # create a dataframe of the 36 individual category columns
    categories = df["categories"].str.split(pat=";", n=36, expand=True)

    # select the first row of the categories dataframe
    row = categories.loc[0,:]
    category_colnames = row.str.slice(stop=-2)

    # rename the columns of `categories`
    categories.columns = category_colnames

    for column in categories:

        # set each value to be the last character of the string
        categories[column] = categories[column].str.slice(start=-1)

        # convert column from string to numeric
        categories[column] = pd.to_numeric(categories[column]) 

    # drop the original categories column from `df`
    df.drop(['categories'], axis=1, inplace=True)    

    # concatenate the original dataframe with the new `categories` dataframe
    df = pd.concat([df, categories], axis=1) 

    # drop duplicates
    df.drop_duplicates(keep="first", inplace=True)
    
    return df

def save_data(df, database_filename):
    '''
    INPUT:
    df (pandas dataframe) - a dataframe to save
    database_filename (string) - filename and its directory
    
    OUTPUT:
    
    DESCRIPTION:
    Save the dataframe into sql database
    '''

    engine = create_engine('sqlite:///'+database_filename)
    df.to_sql('DisasterResponse', engine, index=False)

def main():
    if len(sys.argv) == 4:

        messages_filepath, categories_filepath, database_filepath = sys.argv[1:]

        print('Loading data...\n    MESSAGES: {}\n    CATEGORIES: {}'
              .format(messages_filepath, categories_filepath))
        df = load_data(messages_filepath, categories_filepath)

        print('Cleaning data...')
        df = clean_data(df)
        
        print('Saving data...\n    DATABASE: {}'.format(database_filepath))
        save_data(df, database_filepath)
        
        print('Cleaned data saved to database!')
    
    else:
        print('Please provide the filepaths of the messages and categories '\
              'datasets as the first and second argument respectively, as '\
              'well as the filepath of the database to save the cleaned data '\
              'to as the third argument. \n\nExample: python process_data.py '\
              'disaster_messages.csv disaster_categories.csv '\
              'DisasterResponse.db')


if __name__ == '__main__':
    main()