# import libraries
import sys
import pandas as pd
import numpy as np
from sqlalchemy import create_engine

import pickle
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import word_tokenize

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')

from sklearn.multioutput import MultiOutputClassifier
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import GridSearchCV

def load_data(database_filepath):
    '''
    INPUT:
    database_filepath (string) - database directory and filename 
    
    OUTPUT:
    X (np array) - message text data
    Y (np array) - label for every disaster message
    category_names (list) - list of message categories

    DESCRIPTION: 
    Load table dataframe from saved sql database
    
    '''

    # load data from database
    engine = create_engine('sqlite:///'+database_filepath)
    df = pd.read_sql_table(table_name="DisasterResponse",con=engine)

    # load text data and labels (categories)
    X = df["message"].values#.tolist()
    Y = df[df.columns[4:]].values
    category_names = df.columns[4:].tolist()
    
    return X, Y, category_names

def tokenize(text):
    '''
    INPUT:
    text (string) - text messages 
    
    OUTPUT:
    clean_tokens (list) - list of clean words

    DESCRIPTION: 
    clean and tokenize text

    '''

    lemmatizer = WordNetLemmatizer()

    # normalize case and remove punctuation
    text = re.sub(r"[^a-zA-Z0-9]", " ", text)
    
    # split text by words into list
    tokens = word_tokenize(text)

    # convert text into lowercase and lemetize it
    clean_tokens = []
    for tok in tokens:
        clean_tok = lemmatizer.lemmatize(tok).lower().strip()
        clean_tokens.append(clean_tok)

    return clean_tokens

def build_model():
    '''
    INPUT:
    
    OUTPUT:

    DESCRIPTION: 
    create pipeline to extract feature from text and feed it to the classifier.
    And grid-search some parameters of the extractor and classifier.

    '''

    # create machine learning pipelines
    pipeline = Pipeline([
        ('vect', CountVectorizer(tokenizer=tokenize)),
        ('tfidf', TfidfTransformer(use_idf=True)),
        ('clf', MultiOutputClassifier(RandomForestClassifier(n_estimators=100, min_samples_split=2)))
    ])

    # find best parameters using grid search
    parameters = {
        'vect__ngram_range': ((1, 1), (1, 2)),
        'vect__max_df': (0.5, 0.75, 1.0),
        'vect__max_features': (None, 5000, 10000),
    }    
    
    cv = GridSearchCV(pipeline, param_grid=parameters, n_jobs=4)    
    return cv

def evaluate_model(model, X_test, Y_test, category_names):
    '''
    INPUT:
    model (sklearn model) - trained model 
    X_test (np array) - message text data
    Y_test (np array) - label for every disaster message
    category_names (list) - list of message categories

    OUTPUT:

    DESCRIPTION: 
    test the model performance on each label/categories

    '''
    # predict on test data
    Y_pred = model.predict(X_test)

    # print classificiation report for every category
    for i,col in enumerate(category_names) :
        print(col)
        print(classification_report(Y_test[:,i], Y_pred[:,i], labels=[0,1]))

def save_model(model, model_filepath):
    '''
    INPUT:
    model (sklearn model) - trained model 
    model_filepath (string) - directory and filename for machine learning model  
    
    OUTPUT:

    DESCRIPTION: 
    save the trained model into pickle file

    '''
    pickle.dump(model, open(model_filepath, 'wb'))

def main():

    if len(sys.argv) == 3:
        database_filepath, model_filepath = sys.argv[1:]
        print('Loading data...\n    DATABASE: {}'.format(database_filepath))
        X, Y, category_names = load_data(database_filepath)
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)
        
        print('Building model...')
        model = build_model()
        
        print('Training model...')
        model.fit(X_train, Y_train)
        
        print('Evaluating model...')
        evaluate_model(model, X_test, Y_test, category_names)

        print('Saving model...\n    MODEL: {}'.format(model_filepath))
        save_model(model, model_filepath)

        print('Trained model saved!')

    else:
        print('Please provide the filepath of the disaster messages database '\
              'as the first argument and the filepath of the pickle file to '\
              'save the model to as the second argument. \n\nExample: python '\
              'train_classifier.py ../data/DisasterResponse.db classifier.pkl')


if __name__ == '__main__':
    main()